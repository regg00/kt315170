# kt315170 Test

## Built With

* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)
* [NGINX](https://www.nginx.com/resources/wiki/)


### Prerequisites

Have docker and docker-compose installed on the local system.


### Installation

Nothing to "install". Just copy this repo and run the following commands:

```bash
git clone https://gitlab.com/regg00/kt315170.git
cd kt315170
docker-compose up -d
```

You can validate that everything is up and running with the `docker-compose ps` command. Output should look like this:

```bash
> docker-compose ps
          Name                        Command               State                    Ports
------------------------------------------------------------------------------------------------------------
kt315170_reverse-proxy_1   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp
kt315170_web_1             /docker-entrypoint.sh ngin ...   Up      80/tcp
```

If everything is running properly, you should be able to browse to **https://localhost/uj47G** and accept the self-signed certificate. A login prompt should appear. By default, username is *admin* and password is *Password1*. These can and should be changed in the *docker-compose.yml* file.

#### Protip: You can stop and remove the containers using with `docker-compose down`.

## Explanations

### Why docker?
It was also in the requirements and mostly a must-have by now for any respectable project. With docker you can build your app/code on reliable and easily scalable environments that can be shared with code.

### Why NGINX?
It's open-source, light, fast and easy to configure as a reverse proxy but also as a web server. The base docker images are also easy to use and well documented. It's also the default web server I use for most project since I dropped PHP.

### Why alpine images?
Alpine is a ultra-light Linux OS, so it makes containers much more light and portable. It also reduce build time and attack vector because of it's size and light footprint.
``` bash
> docker images
REPOSITORY                                 TAG                 IMAGE ID            CREATED             SIZE
nginx                                      latest              f35646e83998        3 days ago          133MB
nginx                                      alpine              bd53a8aa5ac9        11 days ago         22.3MB
```
As you can see above, the same docker image using alpine is nearly 80% smaller than the default one.

### Why docker-compose?
I was in the requirements. It's also much more easy for building multi-container applications. No need to worry about networks, link or start script. Everything is already there and easy to use.

Docker compose files can also easily be converted into Kubernetes resources for prod environments.

# Overview of the configuration
## Reverse proxy
### NGINX
Config used was based on a template I had from another project. I simply added the comments on what each options does from memory (mostly).
```bash
# Use only one NGINX process
worker_processes 1;
# Set the max events by worker
events {
    worker_connections 1024;
}

http {

    # Define the SSL certificate and options at the http level so we don't need to copy the same line everywhere if we have multiple websites
    ssl_certificate /etc/nginx/nginx.crt;
    ssl_certificate_key /etc/nginx/nginx.key;
    ssl_ciphers EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH;
    ssl_protocols TLSv1.1 TLSv1.2;

    # Using upstream module make it easier to scale the application in the future. Why not use it right away.
    upstream docker-web {
        server web:80;
        # server web2:80;
        # server web3:80;
    }

    # Redirect every HTTP requests to the appropriate HTTPS URL
    server {
        listen 80 default_server;
        server_name _;
        # Redirect with code 301 to the requested host and uri
        return 301 https://$host$request_uri;
    }

    # Main app server node
    server {
        # Enable the basic authentication with a htpasswd file and name it "Restricted area"
        auth_basic "Restricted area";
        auth_basic_user_file /etc/nginx/nginx.htpasswd;
        listen 443 ssl default_server;

        # Give it a server_name in case we have multiple website that all need to be dispatched on different web server
        server_name test.com;

        # Only valid URL that won't return a 404 error is /uj47G. As per requirements, this is used as a "second factor" for authentication.
        location /uj47G/ {
            # Pass the traffic to the docker-web upstream group
            proxy_pass http://docker-web/;

            # Honestly not really sure what every options does, but it's supposed to help properly redirecting the traffic and all the components of the request to the destination server
            proxy_redirect off;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Host $server_name;
        }
    }
}
```
### Dockerfile
Using the alpine version of docker, I installed the openssl package to create the htpasswd and SSL certificate/key. I then copied the ***nginx.conf*** file in the working directory under */etc/nginx/* as per the docker image [documentation.](https://hub.docker.com/_/nginx) No need to configure anything else here for now. Everything is handled in the config file.
```bash
# Use the alpine version of nginx main docker image
FROM nginx:alpine
LABEL maintainer="Régis Tremblay Lefrançois <regg00@gmail.com>"

# Install the openssl package
RUN apk add openssl

# Create an nginx.htpasswd using openssl. In that case, user is admin and password is Password1
RUN printf "admin:$(openssl passwd -crypt Password1)\n" >> /etc/nginx/nginx.htpasswd

# Generate the self-signed SSL certificate for NGINX
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/nginx.key -out /etc/nginx/nginx.crt -subj "/C=US/ST=NY/L=New-York/O=Mike/OU=Mike/CN=test.com"

# Copy the nginx configuration file inside the container
COPY nginx.conf /etc/nginx/nginx.conf
```

## Web server
### NGINX
Not much config. The base image is configured to serve everything under **/usr/share/nginx/html** by default. I just copied the **index.html** file in the container building process. The default image config remained untouched.

### Dockerfile
Not much config here either. The ***index.html*** file was simply copied under the */usr/share/nginx/html* directory in the container which is configured by default to serve static files under that directoty.
```bash
# Use the alpine version of nginx main docker image
FROM nginx:alpine
LABEL maintainer="Régis Tremblay Lefrançois <regg00@gmail.com>"

# Copy the index.html file to the root directory of the web server inside the container
COPY index.html /usr/share/nginx/html
```

### docker-compose.yml
Pretty basic stuff. No real need for more explanation than the comments in the file.
```yaml
version: "3"
services:

    # Container that will host the main index.html file
    web:    
        # This tell the docker-compose tool to build the Dockerfile inside the ./web directory as a source for the web service. No need to pre-build the image with this.
        build: ./web

    # Where the reverse proxying will be done. Also where the authentication is done.
    reverse-proxy:

        # This tell docker-compose do start the web service before reverse-proxy
        depends_on: 
            - web

        # This tell the docker-compose tool to build the Dockerfile inside the ./reverse-proxy directory as a source for the web service. No need to pre-build the image with this.
        build: ./reverse-proxy
        
        # Expose those ports from the host so that the container can be reached from outside of the docker network
        # Should be written like this <host port>:<container port>
        ports:
            - 80:80
            - 443:443                
```
